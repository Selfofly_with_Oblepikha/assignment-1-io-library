section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 

    mov     rax, 60
    xor     rdi, rdi    
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:

    xor rax, rax
    
  .loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
    
  .end:
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:

    xor rax, rax
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    
  .end:
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    
    mov rdi, 0xA
    
; Принимает код символа и выводит его в stdout
print_char:

    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret



    

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:

    xor rax, rax
    mov rax, rdi
    mov r9, 10
    mov rcx, 0
    jmp .division
    
  .division:
    xor rdx, rdx
    div r9
    push rdx
    inc rcx
    cmp rax, 0
    jne .division
    jmp .collection
    
  .collection:
    pop rdi
    add rdi, 48
    push rcx
    call print_char
    pop rcx
    dec rcx
    cmp rcx, 0
    jne .collection
    ret
    
    

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    
    cmp rdi, 0
    jge print_uint
    neg rdi
    push rdi
    mov rdi, 45
    call print_char
    pop rdi
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    jmp .compare
    
  .inc:
    inc rdi
    inc rsi
    
  .compare:
    mov al, byte[rsi]
    mov dl, byte[rdi]
    
    cmp al, dl
    je .is_null
    mov rax, 0
    ret
    
  .is_null:
    cmp al, 0
    jne .inc
    inc rax
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
  
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - буфер
; rsi - размер

read_word:
    
    push rdi
    push rsi
    push rdi
    push rsi
    
  .search:
    call read_char
    cmp rax, ' '
    je .search
    cmp rax, 0x9
    je .search
    cmp rax, 0xA
    je .search
    pop rsi
    pop rdi
    cmp rax, 0
    je .end
    dec rsi
    mov [rdi], rax
    inc rdi
    jmp .word
    
  .word:
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    cmp rax, 0x20
    je .end
    cmp rax, 0x9
    je .end
    cmp rax, 0xA
    je .end
    cmp rax, 0
    je .end
    dec rsi
    mov [rdi], rax
    inc rdi
    cmp rsi, 0
    jne .word
    pop rsi
    pop rdi
    mov rax, 0
    mov rdx, 0  
    ret
    
  .end:
    mov byte[rdi], 0
    pop rdx
    pop rax
    sub rdx, rsi
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    
    
    mov r9, 10
    xor rdx, rdx
    xor rax, rax
    jmp .check_less
    
  .check_less:
    cmp byte[rdi], 48
    jge .check_greater
    ret
    
  .check_greater:
    cmp byte[rdi], 57
    jle .parse
    ret
    
  .parse:
    push rdx
    mul r9
    pop rdx
    mov r10, [rdi]
    push 0
    mov [rsp], r10b
    pop r10
    add rax, r10
    sub rax, 48
    inc rdx
    inc rdi
    jmp .check_less
    




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    
    push rdi
    call read_char
    pop rdi
    cmp rax, 45
    je .negative
    call parse_uint
    ret
    
  .negative:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    
    xor rax, rax
    push rsi
    push rdi
    push rdx
    call string_length
    pop rdx
    pop rdi
    pop rsi
    cmp rax, rdx
    jl .enough_space
    xor rax, rax
    ret
    
  .enough_space:
    push rax
    push rsi
    push rdi
    push rdx
    call print_string
    pop rdx
    pop rdi
    pop rsi
    pop rax
    xor rax, rax
    jmp .save
    
  .save:
    lea r9, [rdi + rax]
    mov [rsi], r9
    cmp byte [rdi + rax], 0
    je .end
    inc rax
    inc rsi
    
  .end:
    ret
    
    

